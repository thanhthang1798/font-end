import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { VieclamtheonganhngheComponent } from './vieclamtheonganhnghe.component';

describe('VieclamtheonganhngheComponent', () => {
  let component: VieclamtheonganhngheComponent;
  let fixture: ComponentFixture<VieclamtheonganhngheComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ VieclamtheonganhngheComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VieclamtheonganhngheComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
